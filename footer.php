<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WF College Two
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
	<?php get_sidebar( 'footer' ); ?>
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'http://www.wfu.edu/', 'wf-college-two' ) ); ?>"><?php printf( __( '&copy; %s Wake Forest University', 'wf-college-two' ), '2015' ); ?></a>
<!-- lines commented out to remove the theme name and author name
			<span class="sep"> | </span>
			<?php /**
			printf( __( 'Theme: %1$s by %2$s.', 'wf-college-two' ), 'WF College Two', '<a href="http://college.wfu.edu/itg/robert-vidrine" rel="designer">Robert Vidrine</a>' ); **/
			?>
-->
			</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
