<?php
/**
 * The template part for displaying the News Items custom post type.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WF College Two
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'wfco_dept_news' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php wf_college_two_posted_on(); ?>
		FINDME
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->
	<?php
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		?>
		<span class="post_thumbnail">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
			<?php  the_post_thumbnail(); ?>
		</a>
        </span>

		<?php } 	
		?>

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

	<footer class="entry-footer">
		<?php wf_college_two_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
