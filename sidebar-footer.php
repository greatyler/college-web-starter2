<?php
/**
 * The footer sidebar
 *
 */

if ( ! is_active_sidebar( 'sidebar-footer' ) ) { ?>
<div id="supplementary">
	<div id="footer-widgets" class="footer-widgets widget-area clear" role="complementary">

		<aside id="deptcontact" class="widget widget_text" style="flex: 1 1; -webkit-flex: 1 1; margin-right: 20px;">
			<h2 class="widget-title">Department of Demonstration</h2>
			<div class="textwidget">
				<p>Wake Forest University<br />
					Main Office Location<br />
					P.O. Box XXXX, Winston-Salem, NC 27109<br />
					<i class="fa fa-phone"></i><span class="screen-reader-text">Phone: </span> (336) 758.XXXX | <i class="fa fa-fax"></i><span class="screen-reader-text">FAX: </span> (336) 758.XXXX | <span class="screen-reader-text">Email: </span> <a href="mailto:DEPTEMAIL@wfu.edu">DEPTEMAIL@wfu.edu</a></p>
					<p>CHANGE THESE VALUES IN SIDEBAR-FOOTER.PHP</p>
					<!-- After you change the department stuff to your own values, delete the paragraph above this comment, and this comment. -->
			</div>
		</aside>
		<aside id="campuslinks" class="widget widget_text">
			<h2 class="widget-title">Campus Links</h2>
			<div class="textwidget">
				<div>
					<p>
						<a href="http://college.wfu.edu/">WFU College</a>
					</p>
					<p>
						<a href="http://zsr.wfu.edu">ZSR Library</a>
					</p>
					<p>
						<a href="http://www.wfu.edu/academics/calendars/">
							Academic Calendar
						</a>
					</p>
					<p>
						<a href="https://win.wfu.edu/">WIN</a>
					</p>
				</div>
			</div>
		</aside>
	</div><!-- #footer-sidebar -->
</div><!-- #supplementary -->

		<?php }
else { ?>

<div id="supplementary">
	<div id="footer-widgets" class="footer-widgets widget-area clear" role="complementary">
		<?php dynamic_sidebar( 'sidebar-footer' ); ?>
	</div><!-- #footer-sidebar -->
</div><!-- #supplementary -->
<?php }