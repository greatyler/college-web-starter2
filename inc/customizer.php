<?php
/**
 * WF College Two Theme Customizer
 *
 * @package WF College Two
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function wf_college_two_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->add_setting( 'color_scheme', array(
		'type'			=>	'theme_mod',
		'default'		=>	'plain',
		'transport'	=>	'postMessage'
	) );	
	$wp_customize->add_control( 'color_scheme', array(
		'type'			=>	'radio',
		'choices'	=>	array(
			'default'	=> 'Basic White',
			'style-one'		=>	'Style 1'
			),
		'label'        => __( 'Select Color Scheme', 'wfct-child-one' ),
		'section'    => 'colors',
		'setting'   => 'color_scheme',
	) );

	}
add_action( 'customize_register', 'wf_college_two_customize_register' );

/* Output the value of the theme_mod color-scheme to then put it into the body classes */
function wf_college_two_colorscheme_body_class( $classes ) {
	$colorscheme = get_theme_mod( 'color_scheme' );
	$classes[] = 'color-scheme-' . $colorscheme;
	return $classes;
}
/* Put the correct color scheme class into the body classes */
add_filter( 'body_class', 'wf_college_two_colorscheme_body_class' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function wf_college_two_customize_preview_js() {
	wp_enqueue_script( 'wf_college_two_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'wf_college_two_customize_preview_js' );
