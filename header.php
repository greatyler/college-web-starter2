<?php
/**
 * The header for the Politics child theme.
 *
 * Displays all of the <head> section and everything up till <div id="content"> Also loads the meteor slideshow template part on 
  * the front page.
 *
 * @package WF College Two
 */

/* Same as WF College Two hader, but with conditional to get the
** Meteor slideshow template part, meteor-slideshow.php if it's the
** front page */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> >
		<div id="page" class="hfeed site">
			<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'wf-college-two' ); ?></a>
			<header id="masthead" class="site-header" role="banner">
				<?php if ( get_header_image() && ('blank' == get_header_textcolor()) ) { //Have header image but title text hidden ?>
					<figure class="header-image">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
							<img src="<?php header_image(); ?>" width="< ?>" height="<?php echo get_custom_header()->height; ?>" alt="">
						</a>
					</figure>
				<?php } // End header image check. 
				if ( get_header_image() && !('blank' == get_header_textcolor()) ) { //Have header image title text not hidden
				echo '<div class="site-branding header-background-image" style="background-image: url(' . get_header_image() . '); background-size: 960px 200px;">'; 
				} else {
				echo '<div class="site-branding">';
				} ?>
				<div class="title-box">
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
				</div>
			</div>
<?php
/* Put the code to call the meteor slideshow. */
if ( ! wp_is_mobile() ) { ?>
	<div id="masthead-slider-container">
		<?php if ( function_exists( 'meteor_slideshow' ) ) { 
			meteor_slideshow();
		}
}
 ?>		</div>

				<nav id="site-navigation" class="main-navigation" role="navigation">
					<button class="menu-toggle">
					<i class="fa fa-bars"></i>
					</button>
					<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
				<div id="search-container" class="search-box-wrapper clear">
					<div class="search-box clear">
						<?php get_search_form(); ?>
					</div>
				</div>
					<div class="search-toggle">
						<i class="fa fa-search"></i>
						<a href="#search-container" class="screen-reader-text"><?php _e( 'Search', 'wf-college-two' ); ?></a>
					</div>
					<?php wf_college_two_social_menu(); ?>
				</nav><!-- #site-navigation -->
				
		<?php 
		/* Insert a breadcrumb trail if we're on the front page, if one
		** exists.	*/
 				
		if ( function_exists( 'breadcrumb_trail' ) ) {?>
				<div id="breadcrumb-container">
<?php breadcrumb_trail( array( 
	'separator' => '&rarr;',
	'front_page' => false ) ); ?>	
	</div>
<?php } ?>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
