<?php
/**
 * @package WF College Two
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<div class="entry-meta">
			<?php wf_college_two_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
        	<?php 
		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
		?>
		<span class="post_thumbnail">
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
			<?php  the_post_thumbnail(); ?>
		</a>
        </span>

		<?php } 	
		?>
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->


<footer class="entry-footer continue-reading">
    <?php echo '<a href="' . get_permalink() . '" title="' . __('Continue Reading ', 'wf-college-two') . get_the_title() . '" rel="bookmark">Continue Reading<i class="fa fa-arrow-circle-o-right"></i></a>'; ?>
</footer><!-- .entry-footer -->
                </article><!-- #post-## -->