<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WF College Two
 */
?>
<?php 
	$deacnet_name = get_post_meta( $post->ID, 'deacnet_name', 'true'); // get their LDAP username as entered in custom field
	$user_profile_data = get_user_by( 'email', $deacnet_name . '@wfu.edu' ); // add username to domain, then get user data by email
	$first_name = $user_profile_data->first_name;
	$last_name = $user_profile_data->last_name;
	$title = $user_profile_data->wfco_title; /* Get title field added to user profile */
	$email = $user_profile_data->user_email;
	$website = $user_profile_data->user_url;
	$phone = $user_profile_data->wfco_ophone; // get value of field added in functions.php
	$office = $user_profile_data->wfco_olocation; // another field added
	$research = $user_profile_data->wfco_research_interests; // many people won't populate this field, display below is conditional
?>


<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div id="profArea">
		<div id="profName"><?php echo esc_html( $first_name . ' ' . $last_name ); ?></div>
		<div id="profTitle"><?php echo esc_html( $title ); ?></div>
		<div id="profPicArea">
			<div id="profPicIndent">
				<?php echo get_wp_user_avatar( $email ); ?>
			</div>
		</div>
		<div class="profContact">
			<div id="office"><span class="label">Office:</span> <span class="profilefield officlocation"><?php echo esc_html( $office ); ?></span></div>
			<div id="email"><span class="label">Email:<span> <span class="profilefield email"><?php echo '<a href="' . esc_attr( $email ) . '" >' . esc_html( $email ) . '</a>'; ?></span></div>
			<div id="phone"><span class="label">Phone:</span> <span class="profilefield phone"><?php echo esc_html( $phone ); ?></span></div>
			<div id="website">
				<?php
					if( $website ) {
					echo '<span class="label">Website:</span> <span class="profilefield website">' . '<a href="' . esc_url( $website ) . '">' . $website . '</a></span>'; }; 
				?>
			</div>
			<div id="research">
				<?php if( $research ) {
					echo '<span class="label">Research Interests:</span> <span class="profilefield research">' . esc_html( $research ) . '</span>';
				}; ?>
			</div>
			<div id="additionalprofinfo">
				<?php if( ! empty( $post->post_content ) ) {
				echo '<p class="label">More about me: </p>';
				the_content(); } ?>
			</div>
		</div> <!-- End of profContact -->
	</div> <!-- end of profArea -->

	<footer class="entry-footer">
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
