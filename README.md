Welcome to the WF College Two theme!
This theme is designed for use by academic and administrative departments and organizations at Wake Forest University. While the theme is licensed under the GPL, so anyone is free to use it, edit it, and create their own works from it (those works must also be licensed under the GPL), there are lots of design and functionality choices that have been made to accomodate only the needs of a very specific audience.

Changelog:
1.6.3 - Remove the <title> tag from the header.php and implement the standard
	title-tag theme support feature, now required by the WordPress theme standard.
1.6.2 - Removed the font size from the automatically-inserted email icon before all mailto links, and the left padding, so that it would fit naturally in more locations without breaking the flow of the line.
1.6.1 - Fixed an error in which the email address of each faculty member was not being set in the faculty profile template.
1.6 - Added the Faculty Profile template from WF College One Pro so that the information from the user profile will automatically populate the fields on the Faculty Profile page.
1.5.3 - Fixed a bug where third-level menus were flying out on the mobile menu.
1.5.2 - Commented out the section in the footer.php that named the name of the theme and the theme author.
1.5.1 - Removed the masonry.js javascript, as it was difficult to override, and I'm frankly not sure what problem it solves.
1.5 - Enqueue parent stylesheet via functions.php. Usually child stylesheet will be automatically applied when child activated. Removes need for @import in child style sheet.
1.4 - March 5, 2015
	* Changed the header image size (custom-header.php) back to 1280 x 300px. (Need to figure out good way to allow child themes to easily modify this.)
	* Removed support for post formats. I don't use them, don't plan to use them, and decided I didn't need to support the basic one I was before.
	* Removed Hybrid Core. I like Hybrid Core, and used it in my previous theme (WF College One Pro), but since the paradigm for themes has changed (WP recommends putting special functionality in plugins, not themes), the features I wanted were in separate plugins from Justin Tadlock anyway.
	* Removed wf-college-news.php. I don't think my custom post type was really necessary after all, and though I've separated it from my theme so others can continue to use it, I'll just leave it as a possible MU plugin instead of including it with the theme.