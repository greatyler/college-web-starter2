<?php
/**
 * The header sidebar
 *
 */
if ( is_active_sidebar( 'sidebar-header' ) ) : {
?>
<div id="header-widgets" class="header-widgets widget-area clear" role="complementary">
	<?php dynamic_sidebar( 'sidebar-header' ); ?>
</div><!-- #header-sidebar -->

<?php  } else : { // do something if there aren't widgets ?> 
<!-- If you don't want anything to appear in the header widget area, copy sidebar-header.php into your child theme and delete the following div. -->
<div id="header-widgets" class="header-widgets widget-area clear" role="complementary">
<aside class="widget">	
	<ul>
	  <li id="loginout_link">
		<?php wp_loginout( get_permalink(),1); ?>
	  </li>
		<?php wp_register( '<li id="register_link">', '</li>',1); ?>
	  <li class="univlink">
		<a title="Wake Forest University Home" href="http://www.wfu.edu/">Wake Forest University</a>
	  </li>
	  <li class="collegelink">
		<a title="Wake Forest College of Arts and Sciences" href="http://college.wfu.edu/">Wake Forest College</a>
	  </li>
	</ul>
</aside>
</div><!-- #header-widgets-->
 <!-- Delete the above div if you want no content in your header widget area -->
<?php };
endif; ?>
