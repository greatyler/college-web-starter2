<?php

/* Theme PHP code will go here. */

/**
 * WF College Two functions and definitions
 *
 * @package WF College Two
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'wf_college_two_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wf_college_two_setup() {
	/* Add theme support fo rhe title_tag feature, now required of all themes to
	meet the theme standards.	*/
	add_theme_support( 'title-tag' );
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on WF College Two, use a find and replace
	 * to change 'wf-college-two' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'wf-college-two', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/* set the size of the post thumbnails for all featured image locations */
set_post_thumbnail_size (150, 150);

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'wf-college-two' ),
		'social' => __( 'Social Menu', 'wf-college-two'),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'wf_college_two_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // wf_college_two_setup
add_action( 'after_setup_theme', 'wf_college_two_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
 
 function wf_college_two_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Main Sidebar', 'wf-college-two' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Footer Widgets', 'wf-college-two' ),
		'id'            => 'sidebar-footer',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
		register_sidebar( array(
		'name'          => __( 'Header Sidebar', 'wf-college-two' ),
		'id'            => 'sidebar-header',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( 'Faculty Pages Sidebar', 'wf-college-two'),
		'id'            => 'sidebar-faculty',
		'description'   => __( 'Sidebar used only on the Faculty Listing page template', 'wf-college-two' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'wf_college_two_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function wf_college_two_scripts() {

    wp_enqueue_style( 'wf-college-two-core-style' , get_template_directory_uri() . '/style.css');
	
if (is_page_template('page-nosidebar.php')) {
    wp_enqueue_style( 'wf-college-two-layout-style' , get_template_directory_uri() . '/layouts/nosidebar.css');
} else {
    wp_enqueue_style( 'wf-college-two-layout-style' , get_template_directory_uri() . '/layouts/content-sidebar.css');
}
	wp_enqueue_style( 'wf-college-two-font-awesome', 'http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' );
	
		wp_enqueue_script( 'wf-college-two-superfish', get_template_directory_uri() . '/js/superfish.min.js', array('jquery'), '20141113', true );
	

	wp_enqueue_script( 'wf-college-two-superfish-settings', get_template_directory_uri() . '/js/superfish-settings.js', array('wf-college-two-superfish'), '20141113', true );

	wp_enqueue_script( 'wf-college-two-hover-intent', get_template_directory_uri() . '/js/hoverIntent.js', array('wf-college-two-superfish'), '20141113', true );	
	
	wp_enqueue_script( 'wf-college-two-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'wf-college-two-hide-search', get_template_directory_uri() . '/js/hide-search.js', array(), '20141117', true );

	wp_enqueue_script( 'wf-college-two-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wf_college_two_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';


/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/* This function, is_tree checks to see if a page is ANY ancestor of another page. Function is from http://codex.wordpress.org/Conditional_Tags */
  function is_tree($pid) {      // $pid = The ID of the page we're looking for pages underneath
    global $post;         // load details about this page
    $anc = get_post_ancestors( $post->ID );
    foreach($anc as $ancestor) {
      if(is_page() && $ancestor == $pid) {
        return true;
      }
    }
    if(is_page()&&(is_page($pid)))
                 return true;   // we're at the page or at a sub page
    else
                 return false;  // we're elsewhere
  };
/**
 * Add facultyprofilefields.php which adds some fields to the user profile, to support the
  * faculty profile template, which brings in the data from the user profile.
  * Styles for the faculty profile template are in section 11.5 of the style sheet.
  */
require get_template_directory() . '/inc/facultyprofilefields.php';
